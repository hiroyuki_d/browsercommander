package jp.co.ctp.browsercommander;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

/**
 * ファイルを読み込み、コマンドをひとつずる返却するクラス。
 * 
 * @author hdogase1
 * 
 */
public class TaskLoop {

	private static final String COMMAND_SHEET_NAME = "Commands";

	private int counter = 1;

	/**
	 * ワークブックインスタンス
	 */
	private Workbook wb;

	/**
	 * @return the wb
	 */
	private Workbook getWb() {
		return wb;
	}

	/**
	 * @param wb
	 *            the wb to set
	 */
	private void setWb(Workbook wb) {
		this.wb = wb;
	}

	/**
	 * コンストラクタ
	 * 
	 * @param filePath
	 *            クラスに格納しておくファイルのパス
	 */
	public TaskLoop(String filePath) {
		try {
			InputStream inp = new FileInputStream(filePath);
			setWb(WorkbookFactory.create(inp));
			inp.close();
		} catch (InvalidFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 格納しているファイルのステップをひとつずつ進めて、文字列配列を返却する
	 * 
	 * @return 行が存在していれば、その行の左から順番に並べられた文字列変数。行が存在しなければ、null。
	 */
	public String[] progress() {
		Sheet sheet = getWb().getSheet(COMMAND_SHEET_NAME);
		Row row = sheet.getRow(counter++);
		if (row != null) {
			int cellNum = row.getLastCellNum();
			String[] strings = new String[cellNum];
			for (int i = 0; i < cellNum; i++) {
				Cell cell = row.getCell(i);
				if (cell != null) {
					if (cell.getCellType() == 0) {
						strings[i] = String.valueOf((int)cell.getNumericCellValue());
					} else {
						strings[i] = cell.getStringCellValue();
					}
				}
			}
			return strings;
		}
		return null;
	}

	/**
	 * 次の有効行が存在するかどうかを返却
	 * 
	 * @return
	 */
	public boolean hasNext() {
		return counter <= getWb().getSheet(COMMAND_SHEET_NAME).getLastRowNum();
	}

}
