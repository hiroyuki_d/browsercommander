package jp.co.ctp.browsercommander.commands;

import org.openqa.selenium.WebDriver;

public class NonOperation extends Command {

	public NonOperation(WebDriver driver, String[] args) {
		super(driver, args);
	}

	@Override
	protected Object execute() {
		// Non-Operation
		return null;
	}

}
