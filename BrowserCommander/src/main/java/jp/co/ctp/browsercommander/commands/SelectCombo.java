package jp.co.ctp.browsercommander.commands;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class SelectCombo extends Command {

	private static final String VALUE_KEY = "value";

	public SelectCombo(WebDriver driver, String[] args) {
		super(driver, args);
	}

	@Override
	protected Object execute() {
		Select selectObject = new Select(getDriver().findElement(
				By.xpath(getArgs()[1])));

		List<WebElement> list = selectObject.getOptions();

		for (int i = 0; i < list.size(); i++) {
			WebElement elem = list.get(i);

			Pattern p = Pattern.compile(getArgs()[2]);

			Matcher m = p.matcher(elem.getText());

			if (m.find()) {
				selectObject.selectByValue(elem.getAttribute(VALUE_KEY));
				break;
			}
		}
		return null;
	}
}
