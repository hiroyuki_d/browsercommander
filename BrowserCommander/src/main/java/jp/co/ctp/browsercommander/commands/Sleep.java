package jp.co.ctp.browsercommander.commands;

import org.openqa.selenium.WebDriver;

public class Sleep extends Command {

	public Sleep(WebDriver driver, String[] args) {
		super(driver, args);
	}

	@Override
	protected Object execute() {
		try {
			Thread.sleep(1000);
		} catch (NumberFormatException e) {
			// 数値にパースできない引数
			e.printStackTrace();
		} catch (InterruptedException e) {
			// 中断された場合
			e.printStackTrace();
		}
		return null;
	}

}
