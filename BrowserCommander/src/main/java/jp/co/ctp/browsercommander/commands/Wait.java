package jp.co.ctp.browsercommander.commands;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.openqa.selenium.WebDriver;

public class Wait extends Command {

	public Wait(WebDriver driver, String[] args) {
		super(driver, args);
	}

	@Override
	protected Object execute() {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		try {
			if (getArgs().length < 3) {
				br.readLine();
			} else {
				do {
					System.out.println(getArgs()[2]);
				} while (!getArgs()[1].equals(br.readLine()));
			}
		} catch (IOException e) {

		}
		return null;
	}
}
