package jp.co.ctp.browsercommander.commands;

import org.openqa.selenium.WebDriver;

public class Echo extends Command {

	public Echo(WebDriver driver, String[] args) {
		super(driver, args);
	}

	@Override
	protected Object execute() {
		// 第二引数以降が入力されていた場合は改行して出力
		if (getArgs().length < 3) {
			System.out.print(getArgs()[1]);
		} else {
			System.out.println(getArgs()[1] == null ? "" : getArgs()[1]);
		}
		return null;
	}

}
