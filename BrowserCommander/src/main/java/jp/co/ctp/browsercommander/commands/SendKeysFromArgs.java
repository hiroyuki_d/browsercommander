package jp.co.ctp.browsercommander.commands;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SendKeysFromArgs extends Command {

	public SendKeysFromArgs(WebDriver driver, String[] args) {
		super(driver, args);
	}

	@Override
	protected Object execute() {
		getDriver().findElement(By.xpath(getArgs()[1])).clear();
		getDriver().findElement(By.xpath(getArgs()[1])).sendKeys(
				getGlobalArgs()[Integer.parseInt(getArgs()[2])]);
		return null;
	}

}
