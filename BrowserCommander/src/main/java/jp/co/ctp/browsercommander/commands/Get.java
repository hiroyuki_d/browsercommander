package jp.co.ctp.browsercommander.commands;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Get extends Command {

	public Get(WebDriver driver, String[] args) {
		super(driver, args);
	}

	@Override
	protected Object execute() {
		System.out.print(getDriver().findElement(By.xpath(getArgs()[1]))
				.getText());
		return null;
	}

}
