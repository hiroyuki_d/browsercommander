package jp.co.ctp.browsercommander.commands;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SendKeys extends Command {

	public SendKeys(WebDriver driver, String[] args) {
		super(driver, args);
	}

	@Override
	protected Object execute() {
		getDriver().findElement(By.xpath(getArgs()[1])).clear();
		getDriver().findElement(By.xpath(getArgs()[1])).sendKeys(getArgs()[2]);

		String tmp = getDriver().findElement(By.xpath(getArgs()[1])).getText();
		System.out.println(getArgs()[2]);
		return null;
	}

}
