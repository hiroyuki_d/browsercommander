package jp.co.ctp.browsercommander.commands;

import org.openqa.selenium.WebDriver;

public class Open extends Command {

	public Open(WebDriver driver, String[] args) {
		super(driver, args);
	}

	@Override
	protected Object execute() {
		outLog("Open" + getArgs()[1]);
		getDriver().get(getArgs()[1]);
		return null;
	}

}
