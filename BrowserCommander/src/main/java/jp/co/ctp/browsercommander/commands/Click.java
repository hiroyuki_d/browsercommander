package jp.co.ctp.browsercommander.commands;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Click extends Command {

	public Click(WebDriver driver, String[] args) {
		super(driver, args);
	}

	@Override
	protected Object execute() {
		if (getArgs().length >= 3
				&& getArgs()[2].equalsIgnoreCase("persistent")) {
			// 粘り強いクリックイベント
			int cnt = 0;
			while (cnt < 10) {
				try {
					getDriver().findElement(By.xpath(getArgs()[1])).click();
					if (cnt != 0)
						System.err.println("");
					break;
				} catch (Exception ex) {
					try {

						if (cnt != 0) {
							for (int i = 0; i < 29; i++) {
								System.err.print("\b");
							}
						}
						System.err.print("Persistent Click [");
						for (int i = 0; i < cnt; i++) {
							System.err.print("=");
						}
						System.err.print(">");
						for (int i = 0; i < 10 - cnt - 1; i++) {
							System.err.print(" ");
						}
						System.err.print("]");
						Thread.sleep(500);
					} catch (NumberFormatException e) {
						// 数値にパースできない引数
						e.printStackTrace();
					} catch (InterruptedException e) {
						// 中断された場合
						e.printStackTrace();
					}
				}
				cnt++;
				if (cnt == 10)
					System.err.println("");
			}
		} else {
			getDriver().findElement(By.xpath(getArgs()[1])).click();
		}
		return null;
	}
}
