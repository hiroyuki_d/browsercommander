package jp.co.ctp.browsercommander.commands;

import java.lang.reflect.Constructor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

/**
 * 抽象コマンドクラス<BR>
 * コマンドに対する引数を格納する。
 * 
 * @author hdogase1
 * 
 */
public abstract class Command {

	private static String[] globalArgs;

	protected static String[] getGlobalArgs() {
		return globalArgs;
	}

	private static void setGlobalArgs(String[] globalArgs) {
		Command.globalArgs = globalArgs;
	}

	/**
	 * コマンドに共通で使用可能な引数を設定
	 * 
	 * @param args
	 */
	public static void init(String[] args) {
		setGlobalArgs(args);
	}

	private WebDriver driver;

	/**
	 * @return the driver
	 */
	protected WebDriver getDriver() {
		return driver;
	}

	/**
	 * @param driver
	 *            the driver to set
	 */
	protected void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	private String[] args = null;

	/**
	 * @return the args
	 */
	protected String[] getArgs() {
		return args;
	}

	/**
	 * @param args
	 *            the args to set
	 */
	private void setArgs(String[] args) {
		this.args = args;
	}

	public Command(WebDriver driver, String[] args) {
		setDriver(driver);
		setArgs(args);
	}

	/**
	 * コマンド生成ファクトリー
	 * 
	 * @param driver
	 *            コマンド対象ドライバー
	 * @param args
	 *            コマンド生成引数
	 * @return
	 */
	public static Command Factory(WebDriver driver, String[] args) {
		Command ret = new NonOperation(null, null);
		if (args != null) {
			// コマンドパッケージの下にある、コマンドクラスを継承したクラス一覧を取得し、その名称に一致しているコマンドを生成する
			try {
				Class<?> clz = Class.forName(Command.class.getPackage()
						.getName() + "." + args[0]);
				Class<?>[] types = { WebDriver.class, String[].class };
				Constructor<?> constructor = clz.getConstructor(types);
				ret = (Command) constructor.newInstance(new Object[] { driver,
						args });
			} catch (ClassNotFoundException e) {
				// 存在しないコマンドを要求した場合
				System.err.println("Class Not Found:" + args[0]);
			} catch (SecurityException | IllegalArgumentException
					| ReflectiveOperationException e) {
				// リフレクションが失敗した場合
				throw new RuntimeException(e);
			}

		}
		return ret;
	}

	public Object run() {
		try {
			outLog("Execute");
			return execute();
		} catch (Exception e) {
			outLog("Can't find Target" + getArgs()[1]);
		}
		return null;
	}

	protected void outLog(String str) {
		logger.info("[Info][My name is{}]\t{}", this.getClass().getName(), str);
	}

	protected abstract Object execute() throws Exception;

	private static final Logger logger = LogManager.getLogger(Command.class);

}
