package jp.co.ctp.browsercommander.commands;

import org.openqa.selenium.WebDriver;

public class End extends Command {

	public End(WebDriver driver, String[] args) {
		super(driver, args);
	}

	@Override
	protected Object execute() {
		getDriver().quit();
		return null;
	}

}
