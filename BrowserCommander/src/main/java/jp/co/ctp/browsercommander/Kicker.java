package jp.co.ctp.browsercommander;

import jp.co.ctp.browsercommander.commands.Command;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Kicker {
	public static void main(String[] args) {
		setUp();

		Command.init(args);

		// TaskLoopのインスタンスを生成する際に、コマンドシートの文字列を設定し、複数のコマンドリストを生成する。
		// マルチスレッドでドライバーを操作する。
		// ドライバーはスレッド単位で作成する。
		WebDriver driver = new ChromeDriver();
		TaskLoop tl = new TaskLoop(args[0]);

		// 最後の行までループさせる
		while (tl.hasNext()) {
			Command.Factory(driver, tl.progress()).run();
		}

		driver.quit();
	}

	private static void setUp() {
		System.setProperty("webdriver.chrome.driver", "bin/chromedriver.exe");
	}

}
